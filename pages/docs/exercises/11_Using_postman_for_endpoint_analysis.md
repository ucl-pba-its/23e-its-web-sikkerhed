# Opgave 10  - Endpoint analyse med Postman

## Information
I disse øvelser Bliver du introduceret til værktøjet Postman. Postman er et værktøj som
anvendes til at teste API endpoints. Og samtid kan den lave samlinger af forspøgelser til et  
endpoint. 

Der er en del læring i øvelserne, så bevæg dig langsomt frem. Du kommer til at skulle læse op på
værktøjerne mens du udføre øvelserne.

## Instruktioner
Følgende øvelser skal udføres op imod crAPI.

1.  Læs afsnittet [prerequisites](https://github.com/mesn1985/HackerLab/blob/main/crAPI/4_Endpoint_Analysis_with_postman.md#prerequisites).
2.  Udfør [Øvelse 1](https://github.com/mesn1985/HackerLab/blob/main/crAPI/4_Endpoint_Analysis_with_postman.md#1---create-new-collection-in-postman)
3.  Udfør [Øvelse 2](https://github.com/mesn1985/HackerLab/blob/main/crAPI/4_Endpoint_Analysis_with_postman.md#2----capture-the-sign-up-request-send-to-crapi)
4.  Udfør [Øvelse 3](https://github.com/mesn1985/HackerLab/blob/main/crAPI/4_Endpoint_Analysis_with_postman.md#3---bypassing-frontend-validation)
5.  Udfør [Øvelse 4](https://github.com/mesn1985/HackerLab/blob/main/crAPI/4_Endpoint_Analysis_with_postman.md#4---using-postman-as-an-authenticated-user)
6.  Udfør [Øvelse 5](https://github.com/mesn1985/HackerLab/blob/main/crAPI/4_Endpoint_Analysis_with_postman.md#5---create-new-post-in-community) 
7.  Udfør [Øvelse 6](https://github.com/mesn1985/HackerLab/blob/main/crAPI/4_Endpoint_Analysis_with_postman.md#6-create-collection-using-the-browser) 
8.  Udfør [Øvelse 7](https://github.com/mesn1985/HackerLab/blob/main/crAPI/4_Endpoint_Analysis_with_postman.md#7-use-burp-suite-as-a-proxy-for-postman) 
  
Udover overstående øvelser, skal følgende øvelser udføres op imod juice shop  
1. Udfør [Øvelse 1](https://github.com/mesn1985/HackerLab/blob/main/JuiceShop/4_Endpoint_Analysis_with_postman.md#1----using-the-swagger-documentation-of-juiceshop)   
2. Udfør [Øvelse 2](https://github.com/mesn1985/HackerLab/blob/main/JuiceShop/4_Endpoint_Analysis_with_postman.md#2---build-a-juiceshop-collection)   
  
## Links