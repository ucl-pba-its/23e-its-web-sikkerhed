# Opgave 4  - OWASP top 10 Insecure design

## Information
I denne øvelse skal du arbejde med [OWASP top 10 number 4 - Insecure design](https://owasp.org/Top10/A04_2021-Insecure_Design/).
Inde på siden(og dertil hørende links), skal du forsøge at finde svar på de 3 nedstående spørgsmål.

## Instruktioner
1. Hvad bliver overordnet beskrevet i afsnittet _Requirements and Resource management_, og kender du en tilgang som dækker dele af det beskrevet?
2. Hvad menes der med Secure design?
3. I afsnittet _Secure design_ bliver der nævnt en type modellering som bør indgår i en udviklings process. Hvilken type modellering er der her tale om?

  
## Links
