# Opgave 5  - ASVS

## Information
I denne øvelse skal du besvare en række spørgsmål om [OWASP Application Security Verification Standard](https://github.com/OWASP/ASVS/tree/v4.0.3#latest-stable-version---403).
(Du kan finde den seneste udgave ved at klikke på linket)
## Instruktioner
1. I hvilket afsnit kan man finde et overblik over hvad man bør sikre sig ift. input validering?
2. Hvad beskrives der i afsnit _V1 Architecture, Design and threat modelling_
3. I nogen afsnit referes der til _NIST_ , hvad er NIST?

  
## Links
