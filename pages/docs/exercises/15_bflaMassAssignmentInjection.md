# Opgave 16  - Injection, Mass assignment & BFLA. 

## Information
I disse opgaverne skal arbejde med flere forskellige emner. Alle opgaver bygger videre på 
tidligere lærte færdigheder.

Opgaverne er beskrevet i _Hacker lab_ repoet, og delt op i 2 sæt.
Det 1. sæt er grundlæggende angreb mod crAPI, der fungerer som introducerende øvelser,
og det 2. sæt er grundlæggende angreb mod Juiceshop som er repetation samt udvidelse af færdighederne
anvendt i det 1. sæt


## Instruktioner
  
### crAPI øvelser
1. Udfør de tre øvelser i [Basic Injection attack](https://github.com/mesn1985/HackerLab/blob/main/crAPI/8_Basic_Injection_Attacks.md)
2. Udfør øvelsen  i [Mass assignment](https://github.com/mesn1985/HackerLab/blob/main/crAPI/9_Basic_mass_assignment_Attacks.md)
  
### Juiceshop øvelser
1. Udfør de tre øvelser i [Basic Injection attack](https://github.com/mesn1985/HackerLab/blob/main/JuiceShop/8_Basic_Injection_Attacks.md)
2. Udfør øvelsen  i [Mass assignment](https://github.com/mesn1985/HackerLab/blob/main/JuiceShop/9_Basic_mass_assignment_Attacks.md)
3. Udfør de tre øvelser i [Basic BFLA](https://github.com/mesn1985/HackerLab/blob/main/JuiceShop/10_Basic_BFLAS.md)

## Links