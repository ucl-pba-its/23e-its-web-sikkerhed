# Opgave 12  - Angreb på autentificering & password 

## Information
Formålet med disse øvelser en grundlæggende introduktion til hvordan man kan udføre grundlæggende
test af autentificering på web applikationer, som anvender bruger navn og passwords.

Alle øvelserne er beskrevet i hacker lab. Og starter med øvelser op imod crAPI, og skifter herefter over
til juiceshop i den sidste øvelse. Sørg for at læse [prerequisites](https://github.com/mesn1985/HackerLab/blob/main/crAPI/5_basic_Authentication_Attacks.md#prerequisites) afsnittet først.

OBS! øvelse 2 & 3, tager lidt tid at udføre. Så lad de 2 eksekver i baggrunden mens du udfører øvrige øvelser.

## Instruktioner
1. Udfør øvelsen [Dictionary attack with burp suite](https://github.com/mesn1985/HackerLab/blob/main/crAPI/5_basic_Authentication_Attacks.md#dictionary-attack-with-burp-suite)
2. Udfør øvelsen [Dictionary attack with WFuzz](https://github.com/mesn1985/HackerLab/blob/main/crAPI/5_basic_Authentication_Attacks.md#dictionary-attack-with-wfuzz)
3. Udfør øvelsen [Exhaustive brute force  attack with Burpsuite](https://github.com/mesn1985/HackerLab/blob/main/crAPI/5_basic_Authentication_Attacks.md#exhaustive-brute-force-attack-with-burpsuite)
4. Udfør øvelsen [Password spraying](https://github.com/mesn1985/HackerLab/blob/main/crAPI/5_basic_Authentication_Attacks.md#password-spraying)
5. Udfør øvelsen [Brute forcing](https://github.com/mesn1985/HackerLab/blob/main/JuiceShop/5_basic_Authentication_Attacks.md#brute-forcing) _Juice shop_


## Links