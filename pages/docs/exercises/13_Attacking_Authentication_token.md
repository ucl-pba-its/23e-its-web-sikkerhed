# Opgave 13  - Angreb på autentificering & tokens 

## Information
Formålet med disse øvelser en grundlæggende introduktion til hvordan man kan udføre grundlæggende
test af autentificering på web applikationer, som anvender tokens, som uddeles efter en succesfuld autentificering.

Alle øvelserne er beskrevet i hacker lab. Og starter med øvelser op imod crAPI, og skifter herefter over
til juiceshop i den sidste øvelse. Sørg for at læse [prerequisites](https://github.com/mesn1985/HackerLab/blob/main/JuiceShop/6_Basic_Token_Attacks.md#prerequisites) afsnittet først.

Sværhedsgraden i token øvelser er lidt højere end i er vant til, da der er flere trin i disse angreb. Hvis det ikke lykkedes første gang er det helt ok. Så bør i blot
starte forfra og forsøge igen.

## Instruktioner
1. Udfør øvelsen [Getting to know the JWT token](https://github.com/mesn1985/HackerLab/blob/main/crAPI/6_Basic_Token_Attacks.md#getting-to-know-the-jwt-token)
2. Udfør øvelsen [Manipulating the payload](https://github.com/mesn1985/HackerLab/blob/main/crAPI/6_Basic_Token_Attacks.md#manipulate-the-payload)
3. Udfør øvelsen [Abusing unsigned tokens](https://github.com/mesn1985/HackerLab/blob/main/JuiceShop/6_Basic_Token_Attacks.md#manipulating-and-abusing-unsigned-tokens) _Juice shop_

## Links