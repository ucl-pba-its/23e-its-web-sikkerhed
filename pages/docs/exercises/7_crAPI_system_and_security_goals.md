# Opgave 7  - (Gruppe øvelse) crAPI system og sikkerheds mål

## Information
I denne øvelse anvender vi system og sikkerheds mål til at analyser en applikation.
Modsat tidligere øvelser i faget _software sikkerhed_ anvender vi  ikke system og sikkerheds målene til at
skabe overblik over hvordan trussels aktører kan angribe vores system. Men i stedet til hvordan vi selv 
kan misbruge et system, inden vi selv udfører en pentest. 

## Instruktioner
1. Identificer system målene for crAPI (kun web delen, mailhoq kan i ignorer).  
_Husk at identificer betyder at i blot kan nøjes udarbejde et brugstilfælde diagram hvor hvert brugstilfælde har sigende titler_  
1. Identificer sikkerheds målene for hvert system mål.
2. Identificer mulige angrebsflader ved at lave en skitse over applikations arkitekture (Tilsvarende tilgangen med trussels modellering)  
_Da det er en blackbox test, er det kun der API'er som kan identificer fra browser der kan tegnes ind på skitsen. Burp suite eller browserens udvikler værktøjer kan hjælpe med identificer de api'er som browseren sender forspøgelser til_.

## Links
[Intercepting traffic with burp suite](https://www.youtube.com/watch?v=Nr2fYpStshA)
