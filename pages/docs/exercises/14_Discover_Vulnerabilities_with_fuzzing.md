# Opgave 15  - Opdage sårbarheder, med fuzz input 

## Information
Formålet med disse øvelser er at introducer teknikken kaldet fuzzing. I har tidligere arbejdet med fuzz teknikken i forrige
opgave til at udføre dictionary angreb  på autentificering. I disse øvelser skal i anvende teknikken til at detekter SQL
injection sårbarheder.

Øvelserne lægger i hacker lab repoet. Og starter ud med øvelser op imod crAPI, og slutter med øvelser mod juice shop.

## Instruktioner
1. Udfør øvelsen [discovering possible sql injection vulnerability](https://github.com/mesn1985/HackerLab/blob/main/crAPI/7_Fuzzing_input.md#discovering-possible-sql-injection-vulnerability-with-fuzzing)
2. Udfør øvelsen [Discovering possible Juice shop SQL injection vulnerability with fuzzing](https://github.com/mesn1985/HackerLab/blob/main/JuiceShop/7_Fuzzing_input.md#discovering-possible-juice-shop-sql-injection-vulnerability-with-fuzzing)

## Links