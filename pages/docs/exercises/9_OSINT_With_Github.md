# Opgave 9  - OSINT med github

## Information
Kildekoden til en applikation i en offentligt repository på Github, udstiller uhensigtsmæssigt et 
stykke information for meget. Kan du finde den information?

Link til repo: [https://github.com/mesn1985/DotNetApplicationWithToMuchInformation](https://github.com/mesn1985/DotNetApplicationWithToMuchInformation)

## Links
