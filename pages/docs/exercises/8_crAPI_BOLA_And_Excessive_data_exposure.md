# Opgave 8  - BOLA & Excessive data exposure.

## Information
I denne øvelse skal der arbejdes med identificering af sårbarheder.

Øvelserne er beskrevet her [crAPI - BOLA and Excessive data exposure](https://github.com/mesn1985/HackerLab/blob/main/crAPI/2_Exploiting_BOLA_And_Excessive_Data_Exposure.md).

Formålet med øvelserne er at give en grundlæggende introduktion til BOLA sårbarheder, samt excessive data exposure.

Du kan anvende denne tutorial til at lære hvordan man analyser trafik med Burp suite [Intercepting traffic with burp suite](https://www.youtube.com/watch?v=Nr2fYpStshA)

**Det er vigtig at du noterer fremgangsmåden fra alle øvelserne ned, da du skal bruge dem til at udarbejde test cases i næste uge**

## Instruktioner
1. Udfør øvelse [1 - BOLA using object ids without authorization](https://github.com/mesn1985/HackerLab/blob/main/crAPI/2_Exploiting_BOLA_And_Excessive_Data_Exposure.md)  
2. Udfør øvelse [2 - Excessive Data exposure](https://github.com/mesn1985/HackerLab/blob/main/crAPI/2_Exploiting_BOLA_And_Excessive_Data_Exposure.md)  
3. Brug resultaterne fra øvelse 1 og 2 til at finde kørtøjs placeringen fra en bruger du ikke selv har oprettet.  
4. Udfør øvelse [3 - Explorer OWASP Application Security Verification standard.](https://github.com/mesn1985/HackerLab/blob/main/crAPI/2_Exploiting_BOLA_And_Excessive_Data_Exposure.md)  
_Husk at notre resultatet_  
5.  Udfør øvelse [4 - Get access to other customers mechanic reports.](https://github.com/mesn1985/HackerLab/blob/main/crAPI/2_Exploiting_BOLA_And_Excessive_Data_Exposure.md)
6.  (Ekstra opgave) Anvend [OWASP ZAP's](https://www.zaproxy.org/)  [Automatisk skanning](https://www.youtube.com/watch?v=wLfRz7rRsH4) og se om du kan finde yderlige sårbarheder.
7.  (Ekstra opgave) Se om du kan eksekverer flere af dine tidligere identificeret misbrugstilfælde.

## Links
