# Opgave 6  - Web applikation med ASP .Net Core MVC

## Information
I denne opgave skal i implementerer en web applikation der gør brug af en template engine (.Net Razor) til
at generer dynamisk HTML.

Du skal følge og implementerer [Get started with ASP.NET Core MVC](https://learn.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/start-mvc?view=aspnetcore-7.0&tabs=visual-studio-code) tutorialen.
Bemærk at strukturen er meget lig den API applikation i udarbejdet i faget _Software sikkerhed_ i mandags.

**Der er flere sider i denne tutorial, i skal lave frem til og med Add Validation**



## Instruktioner
Der kommer meget information i denne tutorial, men vær særlig opmærksom på følgende:  
- Hvad er en  controller?  
- Hvad er en View?  
- Hvor henne i appliaktionen laves den indledende input validering?  

  
## Links
