# Opgave 3  -  OWASP top 10 Injections

## Information
I denne øvelse skal du arbejde med [OWASP top 10 number 3 - injection](https://owasp.org/Top10/A03_2021-Injection/).
Inde på siden(og dertil hørende links), skal du forsøge at finde svar på de 3 nedstående spørgsmål.

## Instruktioner

1. Læs Beskrivelsen af injection sårbarheden, og beskriv med dine egne ord hvad denne type sårbarhed dækker over.
2. I afsnittet _how to prevent_ bliver en af forstaltningerne mod injection anbgreb, beskrevet som _positive Server side input validation_
, hvad betyder det?
3. I afsnittet _List of mapped CWEs_ fremgår CWE-20, hvad beskriver denne CWE?

## Links
