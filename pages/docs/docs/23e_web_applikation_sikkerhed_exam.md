---
title: '23E ITS web applikation sikkerhed eksamen'
subtitle: 'Eksamens rapport'
filename: '23e_web_applikation_sikkerhed_exam.md'
authors: ['Martin Edwin Schjødt Nielsen \<mesn@ucl.dk\>']
main_author: 'Martin Edwin Schjødt Nielsen'
date: 2023-30-08
email: 'mesn@ucl.dk'
left-header: \today
right-header: Mundtlig eksamen
skip-toc: false
semester: 23E
---

# Dokumentets indhold
Læringsmålene for prøven er identiske med læringsmålene for valgfaget af samme navn og fremgår af valgfags kataloget.


# Eksamens beskrivelse
Prøven er en individuel mundtlig eksamination, der tager udgangspunkt i et af flere kendte spørgsmål.
Den studerende har 10 minutter til præsentation af det emne som spørgsmålet dækker over, og herefter eksamineres den studerende i 10 minutter, der afsættes 5 minutter til votering af afgivelse af bedømmelse.
Den samlet eksaminations tid er 25 minutter inkl. Votering.

# Bedømmelse
Prøven bedømmes efter 7-trinsskalaen ud fra en helheds bedømmelse af præsentationen samt mundtlig eksamenering.

# Mundtlig Eksamens datoer.  
 1. forsøg - 28,29 & 30 November
 2. forsøg - 14 December
 3. forsøg -  3 januar

